<!DOCTYPE hrml>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet"> 
    
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Blooger</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Service</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav> 

<div class="jumbotron">
  <h1>SMK N 3 PARIAMAN</h1>
  <p>Lautan Tantangan,peluang & Sumber Kehidupan</p>
</div>

<section class="my-5">
    <div class="py-5">
        <h2 class="text-center">About Us</h2>
    </div>
    <div class="container-fluids">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/cc4.jpg" class="img-fluid">
        </div>
        <div class="col-lg-6 col-md-6 col-12">
       <p align="center">
       TARUNA SMKN 3 PARIAMAN </p>
       <body>
       <p align="center">
       VISI :</p>
       <p>
       “Melalui lembaga vokasi, wujudkan lulusan yang kompeten, kompetitif,  berkarakter, bertaqwa dan berwawasan lingkungan di era global”</p>
       </body>
       <body>
      <p align="center">
      MISI :</p>
      <p>
      1. Menyiapkan lulusan yang beriman dan bertakwa kepada Tuhan Yang Maha Esa.<br>

      2. Mengoptimalkan pemanfaatan  potensi daerah pencapaian minimal 75%, melalui  sarana dan prasarana pendidikan vokasi yang  berkualitas dan berwawasan lingkungan.<br>

      3. Membekali program pendidikan vokasi yang profesional, berbasis Keahlian Nautika Kapal Penangkap Ikan, Teknika Kapal Penangkap Ikan, Agribisnis Perikanan, Teknik Komputer dan Jaringan, Rekayasa Perangkat Lunak, serta Teknik Pendingin dan Tata Udara, pencapaian 90%.<br>
      </p>
        </div>
    </div>
  </div>
</section>

<p class="p-3 bg-dark text-white text-center">
    @KAKASIMWAH</p>

</body>

</html>