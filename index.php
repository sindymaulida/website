<!DOCTYPE hrml>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet"> 
    
</head>
<body>

<?php  include 'menu.php' ?>


<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/cc1.jpg" alt="Los Angeles" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Los Angeles</h3>
        <p>We had such a great time in LA!</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="images/cc2.jpg" alt="Chicago" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Jangkar</h3>
        <p>Logo Kapal Pesiar</p>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="images/cc3.jpg" alt="New York" width="1100" height="500">
      <div class="carousel-caption">
        <h3>Pantai</h3>
        <p>Sunset pantai gondriah</p>
      </div>   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

<section class="my-5">
    <div class="py-5">
        <h2 class="text-center">About Us</h2>
    </div>
    <div class="container-fluids">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <img src="images/cc4.jpg" class="img-fluid">
        </div>
        <div class="col-lg-6 col-md-6 col-12">
       <p align="center">
       TARUNA SMKN 3 PARIAMAN </p>
       <body>
       <p align="center">
       VISI :</p>
       <p>
       “Melalui lembaga vokasi, wujudkan lulusan yang kompeten, kompetitif,  berkarakter, bertaqwa dan berwawasan lingkungan di era global”</p>
       </body>
       <body>
      <p align="center">
      MISI :</p>
      <p>
      1. Menyiapkan lulusan yang beriman dan bertakwa kepada Tuhan Yang Maha Esa.<br>

      2. Mengoptimalkan pemanfaatan  potensi daerah pencapaian minimal 75%, melalui  sarana dan prasarana pendidikan vokasi yang  berkualitas dan berwawasan lingkungan.<br>

      3. Membekali program pendidikan vokasi yang profesional, berbasis Keahlian Nautika Kapal Penangkap Ikan, Teknika Kapal Penangkap Ikan, Agribisnis Perikanan, Teknik Komputer dan Jaringan, Rekayasa Perangkat Lunak, serta Teknik Pendingin dan Tata Udara, pencapaian 90%.<br>
      </p>
      <a href="about.php" class="btn btn-success"> Check More </a>
        </div>
    </div>
  </div>
</section>

<section class="my-5">
    <div class="py-5">
        <h2 class="text-center">Our services</h2>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
              <div class="card" style="width:400px">
                <img class="card-img-top" src="images/cc5.jpg" alt="Card image">
                    <div class="card-body">
                         <h4 class="card-title">PBB SMKN 3 PARIAMAN</h4>
                            <p class="card-text">Pasukan Baris Berbaris Bersenjata</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
              <div class="card" style="width:400px">
                <img class="card-img-top" src="images/cc5.jpg" alt="Card image">
                    <div class="card-body">
                         <h4 class="card-title">PBB SMKN 3 PARIAMAN</h4>
                            <p class="card-text">Pasukan Baris Berbaris Bersenjata</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
              <div class="card" style="width:400px">
                <img class="card-img-top" src="images/cc5.jpg" alt="Card image">
                    <div class="card-body">
                         <h4 class="card-title">PBB SMKN 3 PARIAMAN</h4>
                            <p class="card-text">Pasukan Baris Berbaris Bersenjata</p>
                    </div>
                </div>
            </div>
          </div>
    </div>

</section>

<section class="my-5">
    <div class="py-5">
        <h2 class="text-center">Our Gallery</h2>
    </div>

    <div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc6.jpg" class="img-fluid pb-4">
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc7.jpg" class="img-fluid pb-4">
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc8.jpg" class="img-fluid pb-4">
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc9.jpg" class="img-fluid pb-4">
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc10.jpg" class="img-fluid pb-4">
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <img src="images/cc11.jpg" class="img-fluid pb-4">
              </div>
            </div>
    </div>
</section>

<section class="my-5">
    <div class="py-5">
        <h2 class="text-center">Our Gallery</h2>
    </div>

    <div class="w-50 m-auto">
        <form action="userinfo.php" method="post">
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="user" autocomplete="off" class="form-control">
          </div>
          <div class="form-group">
            <label>Email Id</label>
            <input type="text" name="email" autocomplete="off" class="form-control">
          </div>
          <div class="form-group">
            <label>Mobile</label>
            <input type="text" name="user" autocomplete="off" class="form-control">
          </div>
          <div class="form-group">
            <label>Comments</label>
            <textarea class="form-control" name="comments">
            </textarea>
          </div>
          <button type="submit" class="btn btn-success">Submit</button>
        </from>
      </div>

</section>

<footer>
    <p class="p-3 bg-dark text-white text-center">
    @KAKASIMWAH</p>
</footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>